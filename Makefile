build:
	docker-compose build

restart:
	docker-compose restart ${service}

run:
	docker-compose up

logs:
	docker-compose logs

remove:
	docker-compose rm -svf

stop:
	docker-compose stop

lint:
	python3 -m pylint api

dev:
	docker-compose up --build

run-dev: build run

clean: stop remove
