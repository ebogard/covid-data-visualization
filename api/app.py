import os
import json
import csv
import requests
from hashlib import md5
from flask import Flask
from pymongo import MongoClient

class Encoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj, ObjectId):
            return str(obj)
        return super(Encoder, self).default(obj) 

app = Flask(__name__)
app.json_encoder = Encoder
client = MongoClient(os.environ['MONGO_CONNECT'])
default_db = client['admin']
covid_db = client['covid']


@app.route('/status')
def get_status():
    col = default_db['system.version']
    return col.find_one({'_id':'authSchema'})
    
# Fetches latest data and replaces existing mongodb collection
# data resource https://healthdata.gov/api/views/j8mb-icvb/rows.csv
@app.route('/reconcile')
def reconcile():
    r = requests.get('https://healthdata.gov/api/views/j8mb-icvb/rows.csv')
    with open('temp', 'wb') as file:
        file.write(r.content)
        file.close
    with open('temp', 'r') as file:
        csv_reader = csv.reader(file, delimiter=',')
        columns = next(csv_reader)
        rows = []
        for item in csv_reader:
            final_obj = {}
            for idx, cn in enumerate(columns):
                final_obj[cn] = item[idx]
                final_obj['_id'] = str(md5((''.join(item)).encode()).hexdigest())
            rows.append(final_obj)
        file.close
    col = covid_db['covid-data']
    col.insert_many(rows)
    return 'success'


    
